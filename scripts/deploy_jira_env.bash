#!/bin/bash
#________________________________________________
# Purpose:                                      |
# Buids and deploys two servers, one for Jira   | 
# and one for Postgresql. Calls additional      |
# scripts for further configuration.            |
#                                               | 
# Author: Caleb I. Hart                         |
#------------------------------------------------
#
# Setting variables
dbvm="chart-postgres"
jiravm="chart-jira"
pgdbx="/home/caleb/pginst.sh"
ZONE="us-central1-a"
pgconf0="/home/caleb/postgresql.conf"
pgconf1="/home/caleb/pg_hba.conf"
# build database instance, let it marinate
gcloud compute instances create $dbvm \
  --image centos-7-v20201112 \
  --image-project centos-cloud
  
sleep 300

# validate instance, then give it purpose
grnlt=$(gcloud compute ssh $dbvm --command="echo success")
if [ $grnlt = "success" ]
then
gcloud compute scp $pgdbx $dbvm:/home/caleb/
gcloud compute scp $pgconf0 $dbvm:/home/caleb/
gcloud compute scp $pgconf1 $dbvm:/home/caleb/
sleep 10
gcloud compute ssh $dbvm --command="sudo chmod 755 $pgdbx ; sudo bash -s $pgdbx"
sleep 240

# make sure we're still alive, then dump db and copy archive to new server
dbval=$(gcloud compute ssh $dbvm --command="grep Connected /home/caleb/nc.out | wc -l")
dbip=$(gcloud compute instances describe $dbvm --format='get(networkInterfaces[0].networkIP)')
  if [[ $dbval == "1" ]]
  then
  pg_dump -h 10.128.0.4 -U jiradbuser jiradbuser > /home/caleb/dbdump.pgsql
  gcloud compute scp /home/caleb/dbdump.pgsql $dbvm:/home/caleb/
  else
  echo "unable to connect to DB at port 5432. You should look at $dbip to fix it"
  fi
else
echo -e"You broke it.\n \n   Do better"
fi

# build Jira instance, let it marinate
gcloud compute instance create $jiravm \
  --image centos-7-v20201112 \
  --image-project centos-cloud
  
sleep 300

# validate instance, then give it purpose
grnlt1=$(gcloud compute ssh $jiravm --command="echo success")
if [ $grnlt1 = "success" ]
then
gcloud compute ssh --command="sudo mkdir /usr/jira ; sudo chown caleb:caleb /usr/jira"
gcloud compute ssh --command="sudo mkdir /etc/jiraconf ; sudo chown caleb:caleb /etc/jiraconf"
gcloud compute scp --recurse /home/caleb/jira $jiravm:/usr/
gcloud compute scp --recurse /home/caleb/jiraconf $jiravm:/etc/
gcloud compute ssh $jiravm --command="sudo bash -s /usr/jira/bin/start-jira.sh"
else
echo "try, try again"
fi