#!/bin/bash
#________________________________________________
# Purpose:                                      |
#                                               |
# Sets up postgresql 11 on centos7,             |
# prepares DB for connection to Jira.           |
#                                               | 
# Author: Caleb I. Hart                         |
#------------------------------------------------
#
#
# Some variables
pgconf0="/home/caleb/postgresql.conf"
pgconf1="/home/caleb/pg_hba.conf"
myip=$(ip a | grep "inet " | tail -1 | awk '{print $4}')

# Installing tools
yum -y update
sleep 90
yum -y install nc

# installing postgres, initializing
sed -i '29i\exclude=postgresql*\' /etc/yum.repos.d/CentOS-Base.repo
yum -y install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
sleep 10
yum -y install postgresql11-server
sleep 10
/usr/pgsql-11/bin/postgresql-11-setup initdb

# more variables, start postgres, validate connectivity
pgconf0dest"/var/lib/pgsql/11/data/postgresql.conf"
pgconf1dest"/var/lib/pgsql/11/data/pg_hba.conf"
cp $pgconf0 $pgconf0dest
cp $pgconf1 $pgconf1dest
systemctl start postgresql-11
systemctl enable postgresql-11
firewall-cmd --add-service=postgresql --permanent
firewall-cmd --reload
nc -4vz $myip 5432 > /home/caleb/nc.out 2>&1
chmod 666 /home/caleb/nc.out
chown caleb:caleb /home/caleb/nc.out
sleep 400

# create database user on machine and on database, import
useradd jiradbuser
su -u postgres -c "psql -c create user jiradbuser with password 'dangerzone';"
su -u postgres -c "psql -c create database jiradbuser;"
su -u postgres -c "psql -c grant all privileges on jiradbuser to jiradbuser;"
su -u jiradbuser -c "psql -c jiradbuser < /home/caleb/dbdump.pgsql